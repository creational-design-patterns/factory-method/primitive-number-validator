# Number Type Validator

## Description
With this tool it's possible to validate whether the value of number might be of a specific type

To choose the type you want to validate the value against it's enough to specify NumberType Enum\
as a parameter of a static method responsible for correct factory instantiation

It's an example of Factory Method usage

## Notes
1) Client doesn't need to know anything about concrete Factories,Validators\
and its internal implementation. They only need to know the type of Number\
they're interested in validating.
2) Since the code is decoupled it's easy to introduce new concrete Factories\
and Product classes if Client needs them
3) Any Client could extend the Factory and use it by direct instantiation\
instead of using a static factory method