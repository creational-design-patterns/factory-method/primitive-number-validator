package product;

public interface NumberTypeValidator {
    void validate(Number number);
}
