package product.concrete;

import product.NumberTypeValidator;

public class NumberValidator implements NumberTypeValidator {
    @Override
    public void validate(Number number) {
        System.out.printf("%s is a valid Number!%n", number);
    }
}
