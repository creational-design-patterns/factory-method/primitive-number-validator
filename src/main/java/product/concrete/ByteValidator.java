package product.concrete;

import product.NumberTypeValidator;

import java.math.BigInteger;

public class ByteValidator implements NumberTypeValidator {
    private final BigInteger MAX_VALUE = new BigInteger(String.valueOf(Byte.MAX_VALUE));
    private final BigInteger MIN_VALUE = new BigInteger(String.valueOf(Byte.MIN_VALUE));

    @Override
    public void validate(Number number) {
        String value = String.valueOf(number.toString());

        try {
            BigInteger valueAsBigInteger = new BigInteger(value);

            boolean isValid = MAX_VALUE.compareTo(valueAsBigInteger) >= 0
                    && MIN_VALUE.compareTo(valueAsBigInteger) <= 0;

            if (isValid) {
                System.out.printf("%s is a valid Byte!%n", number);
            } else {
                System.out.printf("%s isn't a Byte!%n", number);
            }

        } catch (NumberFormatException e) {
            System.out.printf("%s isn't a Byte!%n", number);
        }

    }
}
