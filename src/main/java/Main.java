import enums.NumberType;
import factory.NumberTypeValidatorFactory;
import product.NumberTypeValidator;

import java.math.BigDecimal;
import java.util.Locale;
import java.util.Scanner;

public class Main {

    // An example of Client Code
    public static void main(String[] args) {
        NumberType numberType = NumberType.BYTE;
        NumberTypeValidatorFactory numberTypeValidatorFactory = NumberTypeValidatorFactory.getNumberTypeValidatorFactory(numberType);
        NumberTypeValidator numberTypeValidator = numberTypeValidatorFactory.getNumberTypeValidator();

        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.US);

        System.out.println("Please enter a number you want to validate or 'STOP' to exit");
        String input = scanner.nextLine();
        while (!input.equalsIgnoreCase("STOP")) {
            Number number = new BigDecimal(input);
            numberTypeValidator.validate(number);
            input = scanner.nextLine();
        }
    }
}
