package factory;

import enums.NumberType;
import factory.concrete.ByteValidatorFactory;
import factory.concrete.DoubleValidatorFactory;
import factory.concrete.NumberValidatorFactory;
import product.NumberTypeValidator;


public abstract class NumberTypeValidatorFactory {
    public abstract NumberTypeValidator getNumberTypeValidator();

    public static NumberTypeValidatorFactory getNumberTypeValidatorFactory(NumberType numberType) {
        NumberTypeValidatorFactory numberTypeValidatorFactory;

        switch (numberType) {
            case BYTE -> {
                numberTypeValidatorFactory = new ByteValidatorFactory();
                System.out.printf("Factory implementation for a Byte type validation has been successfully instantiated%n");
            }
            case DOUBLE -> {
                numberTypeValidatorFactory = new DoubleValidatorFactory();
                System.out.printf("Factory implementation for a Double type validation has been successfully instantiated%n");
            }
            case null, default -> {
                numberTypeValidatorFactory = new NumberValidatorFactory();
                System.out.printf("Default factory implementation for a Number type validation has been successfully instantiated%n");
            }
        }

        return numberTypeValidatorFactory;
    }
}
