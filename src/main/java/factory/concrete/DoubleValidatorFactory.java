package factory.concrete;

import factory.NumberTypeValidatorFactory;
import product.NumberTypeValidator;
import product.concrete.DoubleValidator;

public class DoubleValidatorFactory extends NumberTypeValidatorFactory {
    @Override
    public NumberTypeValidator getNumberTypeValidator() {
        DoubleValidator doubleValidator = new DoubleValidator();

        System.out.println("DoubleValidator has been successfully instantiated");

        return doubleValidator;
    }
}
