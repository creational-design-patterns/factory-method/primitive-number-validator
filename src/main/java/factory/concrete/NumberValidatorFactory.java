package factory.concrete;

import factory.NumberTypeValidatorFactory;
import product.NumberTypeValidator;
import product.concrete.NumberValidator;

public class NumberValidatorFactory extends NumberTypeValidatorFactory {

    @Override
    public NumberTypeValidator getNumberTypeValidator() {
        NumberValidator numberValidator = new NumberValidator();

        System.out.println("NumberValidator has been successfully instantiated");

        return numberValidator;
    }
}
