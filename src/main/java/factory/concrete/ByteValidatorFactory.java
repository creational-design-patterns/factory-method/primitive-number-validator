package factory.concrete;

import factory.NumberTypeValidatorFactory;
import product.NumberTypeValidator;
import product.concrete.ByteValidator;

public class ByteValidatorFactory extends NumberTypeValidatorFactory {
    @Override
    public NumberTypeValidator getNumberTypeValidator() {
        ByteValidator byteValidator = new ByteValidator();

        System.out.println("ByteValidator has been successfully instantiated");

        return byteValidator;
    }
}
